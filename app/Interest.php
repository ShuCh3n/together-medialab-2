<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    public function tags()
    {
    	return $this->hasMany(InterestTag::class);
    }
}
