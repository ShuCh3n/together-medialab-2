<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Interest;
use App\Country;
use App\InterestTag;
use App\Hotel;
use GuzzleHttp\Client;

class PageController extends Controller
{
    public function home()
    {
    	return view('home');
    }

    public function steps(Request $request)
    {
    	switch ($request->step) {
    		case 1:
                $request->session()->forget('tags');
                $interests = Interest::all();

    			return view('step1', compact('interests'));
    			break;
    		case 2:
                $request->session()->forget('additional_info');
                $session_tag = array();

                if($request->main_interest)
                {
                    foreach($request->main_interest as $interest_id)
                    {
                        $interest = Interest::findOrFail($interest_id);

                        foreach($interest->tags as $tag)
                        {
                            $session_tag[] = $tag->tag;
                        }
                    }
                }

                if($request->tag)
                {
                    $session_tag[] = $request->tag;
                }

                $request->session()->put('tags', $session_tag);

                $countries = Country::all();

    			return view('step2', compact('countries'));
    			break;
    		case 3:
                $client = new Client();

                $data = array(
                    "form_params" => array(
                        "tags" => session()->get('tags'),
                        "left_longitude" => "3.359403",
                        "right_longitude" => "7.227496",
                        "top_latitude"  => "53.560406",
                        "bottom_latitude" => "50.750938",
                        "location" => true
                    )
                );
                
                $call = $client->request('POST', 'http://socialmedia.shuch3n.com//tags', $data);
                $tag_results = json_decode($call->getBody());

                foreach($tag_results as $result)
                {
                    $tag_info = InterestTag::where('tag', $result->name)->first();

                    if($tag_info)
                    {
                        $result->icon = ($tag_info->icon)? $tag_info->icon : 'other';
                    }
                    else
                    {
                        $result->icon = 'other';
                    }
                }

                $request->session()->put('duration', $request->duration);
                $request->session()->put('person', $request->person);

                $hotels = Hotel::all();

                foreach($hotels as $hotel)
                {
                    switch ($hotel->name) {
                        case 'Ibis':
                            $hotel->icon = 'IBH.jpg';
                            break;
                        case 'Ibis Budget':
                            $hotel->icon = 'IBB.jpg';
                            break;
                        case 'Ibis Styles':
                            $hotel->icon = 'IBS.jpg';
                            break;
                        case 'INK':
                            $hotel->icon = 'MGA.jpg';
                            break;
                        case 'Mercure':
                            $hotel->icon = 'MER.jpg';
                            break;
                        case 'Novotel':
                            $hotel->icon = 'SUI.jpg';
                            break;
                        case 'Pullman':
                            $hotel->icon = 'PUL.jpg';
                            break;
                        case 'Sofitel':
                            $hotel->icon = 'SOL.jpg';
                            break;
                        default:
                            $hotel->icon = null;
                            break;
                    }
                    
                }

    			return view('step3', compact('tag_results', 'hotels'));
    	}
    }
}
