AmCharts.themes.black = {

	themeName: "black",

	// ammap
	AreasSettings: {
		alpha: 0.8,
		color: "#4A748C",
		colorSolid: "#000000",
		unlistedAreasAlpha: 0.4,
		unlistedAreasColor: "#555555",
		outlineColor: "#FFFFFF",
		outlineAlpha: 0.5,
		outlineThickness: 0.5,
		rollOverBrightness: 30,
		slectedBrightness: 50,
		rollOverOutlineColor: "#000000",
		selectedOutlineColor: "#000000",
		unlistedAreasOutlineColor: "#000000",
		unlistedAreasOutlineAlpha: 0.5
	},

	LinesSettings: {
		color: "#555555",
		alpha: 0.8
	},

	ImagesSettings: {
		alpha: 0.8,
		labelColor: "#FFFFFF",
		color: "#FFFFFF",
		labelRollOverColor: "#3c5bdc"
	},

	ZoomControl: {
		buttonFillAlpha: 0.4
	},

	SmallMap: {
		mapColor: "#444444",
		rectangleColor: "#666666",
		backgroundColor: "#000000",
		backgroundAlpha: 0.5,
		borderColor:"#555555",
		borderThickness: 1,
		borderAlpha: 0.8
	},

	// the defaults below are set using CSS syntax, you can use any existing css property
	// if you don't use Stock chart, you can delete lines below
	PeriodSelector: {
		color: "#e7e7e7"
	},

	PeriodButton: {
		color: "#e7e7e7",
		background: "transparent",
		opacity: 0.7,
		border: "1px solid rgba(255, 255, 255, .15)",
		MozBorderRadius: "5px",
		borderRadius: "5px",
		margin: "1px",
		outline: "none",
		boxSizing: "border-box"
	},

	PeriodButtonSelected: {
		color: "#e7e7e7",
		backgroundColor: "rgba(255, 255, 255, 0.1)",
		border: "1px solid rgba(255, 255, 255, .3)",
		MozBorderRadius: "5px",
		borderRadius: "5px",
		margin: "1px",
		outline: "none",
		opacity: 1,
		boxSizing: "border-box"
	},

	PeriodInputField: {
		color: "#e7e7e7",
		background: "transparent",
		border: "1px solid rgba(255, 255, 255, .15)",
		outline: "none"
	},

	DataSetSelector: {
		color: "#e7e7e7",
		selectedBackgroundColor: "rgba(255, 255, 255, .25)",
		rollOverBackgroundColor: "rgba(255, 255, 255, .15)"
	},

	DataSetCompareList: {
		color: "#e7e7e7",
		lineHeight: "100%",
		boxSizing: "initial",
		webkitBoxSizing: "initial",
		border: "1px solid rgba(255, 255, 255, .15)"
	},

	DataSetSelect: {
		border: "1px solid rgba(255, 255, 255, .15)",
		outline: "none"
	}

};