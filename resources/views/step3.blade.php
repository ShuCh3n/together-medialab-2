@extends('layout')

@section('head')
	<link rel="stylesheet" href="/css/ammap.css">
@stop

@section('javascript')
	<script type="text/javascript" src="/js/ammap.js"></script>
	<script src="/js/black.js" type="text/javascript"></script>
	<script type="text/javascript" src="/maps/js/netherlandsHigh.js"></script>
	<script>
		AmCharts.theme = AmCharts.themes.black;

		AmCharts.ready(function() {

		    var map = new AmCharts.AmMap();

		    map.areasSettings = {
		        autoZoom: true,
		        rollOverBrightness:10,
		        selectedBrightness:20
		    };

		    var dataProvider = {
		        mapVar: AmCharts.maps.netherlandsHigh,
		        images: [
		        	@foreach($hotels as $hotel)
		        		{
		        			imageURL: "/hotel_logo/{{ $hotel->icon }}",
		        			zoomLevel: 5,
							scale: 0.5,
							latitude: {{ $hotel->latitude }},
							longitude: {{ $hotel->longitude }},
							width: 20,
							height: 20,
							title: '{{ $hotel->name }}'
		        		},
		        	@endforeach

		        	@foreach($tag_results as $tag)
						@foreach($tag->posts as $key => $post)
							{
								imageURL: "/tag_icons/{{ $tag->icon }}.svg",
								zoomLevel: 5,
								scale: 0.5,
								latitude: {{ $post->latitude }},
								longitude: {{ $post->longitude }},
								width: 20,
								height: 20,
								balloonText: '@if(isset($post->media->id)) <img width="100%" src="http://socialmedia.shuch3n.com/media/media/{{ $post->media->id }}" /> @endif {{ trim(preg_replace("/\s\s+/", " ", addcslashes($post->text,"\\\'\"\n\r"))) }}'
							},
						@endforeach
					@endforeach
		        ]
		    };

		    map.dataProvider = dataProvider;
		    map.write("mapdiv");
		});
	</script>
@stop

@section('content')
	<div id="mapdiv" style="width: 100%; background-color:#DCEBF0; height: 100%;"></div>
@stop