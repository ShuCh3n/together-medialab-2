@extends('layout')

@section('head')
	<link rel="stylesheet" href="/css/nouislider.min.css">
	<link rel="stylesheet" href="/css/select2.min.css">
@stop

@section('javascript')
	<script src="/js/nouislider.min.js"></script>
	<script src="/js/wNumb.js"></script>
	<script src="/js/select2.full.min.js"></script>

	<script>
		var rangeDurationSlider = document.getElementById('slider-duration');
		var rangeDurationSliderValueElement = document.getElementById('slider-duration-value');

		var rangePersonSlider = document.getElementById('slider-person');
		var rangePersonSliderValueElement = document.getElementById('slider-person-value');

		noUiSlider.create(rangeDurationSlider, {
			start: [ 3 ],
			step: 1,
			range: {
				'min': [  1 ],
				'max': [ 10 ]
			},
			format: wNumb({
				decimals: 0
			})
		});

		rangeDurationSlider.noUiSlider.on('update', function( values, handle ) {
			rangeDurationSliderValueElement.innerHTML = values[handle] + ' weeks';
			$('input#duration').val(values[handle]);
		});

		noUiSlider.create(rangePersonSlider, {
			start: [ 2 ],
			step: 1,
			range: {
				'min': [  1 ],
				'max': [ 10 ]
			},
			format: wNumb({
				decimals: 0
			})
		});

		rangePersonSlider.noUiSlider.on('update', function( values, handle ) {
			rangePersonSliderValueElement.innerHTML = values[handle] + ' person';
			$('input#person').val(values[handle]);
		});

		$('select').select2({
			placeholder: 'Destination...'
		});
	</script>
@stop

@section('content')
	<section id="step2">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="margin-top-0 wow fadeIn">Last step, promise!</h2>
                    <hr class="primary">
                </div>

                <form action="{{ route('steps', 3) }}" method="get">
	                <div class="col-lg-10 col-lg-offset-1 text-center">
	                	<div class="mb30"></div>
	                    <p>How long do you want take your break?</p>
	                    <div id="slider-duration-value" class="slider_result_value"></div>
	                    <div id="slider-duration"></div>
	                    <input type="hidden" id="duration" name="duration">
	                </div>

	                <div class="col-lg-10 col-lg-offset-1 text-center">
	                	<div class="mb30"></div>
	                    <p>How many people, you say?</p>
	                    <div id="slider-person-value" class="slider_result_value"></div>
	                    <div id="slider-person"></div>
	                    <input type="hidden" id="person" name="person">
	                </div>

	                <div class="col-lg-10 col-lg-offset-1 text-center">
	                	<div class="mb30"></div>
	                    <p>You know the destination yet?</p>

	                    <select class="form-control text-center" name="country">
	                    	<option value="nl">Netherlands</option>
	                    </select>
	                </div>

	                <div class="col-md-4 col-md-offset-4">
	                	<div class="mb20"></div>
	                    <button type="submit" class="btn btn-primary btn-block btn-lg">Show me <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
	                </div>
	            </form>
            </div>
        </div>
    </section>
@stop