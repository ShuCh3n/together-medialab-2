@extends('layout')

@section('javascript')
  <script>
    $(function(){
      $('#video-background').get(0).play();
    });
  </script>
@stop

@section('content')
    <header id="first">
        <div class="header-content">
            <div class="inner">
                <h1 class="cursive">Together</h1>
                <h4>I'd rather have a pssport full of stamps than a house full of stuff</h4>
                <hr>
                <a href="{{ route('steps', 1) }}" class="btn btn-primary btn-facebook btn-xl page-scroll"><i class="fa fa-facebook" aria-hidden="true"></i> Login with Facebook</a>
            </div>
        </div>
        <video muted loop id="video-background" width="100%">
            <source src="/media/accor_hotel_background.mp4" type="video/mp4">
        </video>
    </header>
@stop
