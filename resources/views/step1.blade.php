@extends('layout')

@section('javascript')
    <script>
        $(function(){
            $('.gallery-box').click(function(){
                var data = $(this).attr('data');

                var checkBox = $(".main_interest[value=" + data + "]");
                checkBox.prop("checked", !checkBox.prop("checked"));

                if(checkBox.prop('checked')) 
                {
                    $(".gallery-box-caption", this).addClass('main_interest_selected');
                } 
                else 
                {
                    $(".gallery-box-caption", this).removeClass('main_interest_selected');
                }
            });
        });
    </script>
@stop

@section('content')

	<section id="step1">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="margin-top-0 wow fadeIn">Your interest</h2>
                    <hr class="primary">
                    <p>Tell us more about you...</p>
                </div>
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    Select the images that seem appealing to you
                </div>
            </div>
        </div>

        <form method="get" action="{{ route('steps', 2) }}">
            <div class="container-fluid">
                <div class="row no-gutter">
                    @foreach($interests as $interest)
                        <div class="col-sm-4 col-sm-6">
                            <a href="#gallery" class="gallery-box" data="{{ $interest->id }}">
                                <img src="/media/{{ $interest->image }}" class="img-responsive">
                                <div class="gallery-box-caption">
                                    <div class="gallery-box-content">
                                        <div>
                                            {{ $interest->name }} <br />
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <input type="checkbox" name="main_interest[]" class="main_interest hidden" value="{{ $interest->id }}">
                    @endforeach
                </div>
            </div>

            <div class="container">
                <div class="mb20"></div>
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="margin-top-0 wow fadeIn">Specific interest</h2>
                        <hr class="primary">
                    </div>
                    <div class="col-lg-10 col-lg-offset-1 text-center">
                        Tell us what you really like during your trip
                    </div>
                </div>
            </div>

            <div class="col-lg-10 col-lg-offset-1 text-center">
                <div class="col-md-4 col-md-offset-4">
                    <input type="text" class="form-control text-center" name="tag" placeholder="#Tag">
                    <div class="mb20"></div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="mt30"></div>
        </form>      
    </section>
@stop