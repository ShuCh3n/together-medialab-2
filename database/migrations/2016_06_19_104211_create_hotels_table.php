<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('latitude', 18, 14);
            $table->decimal('longitude', 18, 14);
            $table->timestamps();
        });

        DB::table('hotels')->insert([
            [
                "name" => "Mercure",
                "latitude"  => "52.506",
                "longitude" => "6.06202"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "51.84202",
                "longitude" => "5.853261"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "51.457844",
                "longitude" => "5.40675"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "51.55682",
                "longitude" => "5.09085"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "51.91739",
                "longitude" => "4.33295"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.11124",
                "longitude" => "4.28109"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "51.54149",
                "longitude" => "5.05103"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "51.566326",
                "longitude" => "4.744392"
            ],
            [
                "name" => "Pullman",
                "latitude"  => "51.43883",
                "longitude" => "5.482166"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "51.915",
                "longitude" => "4.5295"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "51.917477",
                "longitude" =>  "4.48839"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "51.92409",
                "longitude" =>  "4.37531"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "52.0929",
                "longitude" => "4.28397" 
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.07601",
                "longitude" => "4.30729" 
            ],
            [
                "name" => "Novotel",
                "latitude"  => "52.0769",
                "longitude" => "4.31312"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "52.07653",
                "longitude" => "4.317706"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "52.078384",
                "longitude" => "4.312146"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.164085",
                "longitude" => "4.481649"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "52.291626",
                "longitude" =>  "4.701424"
            ],
            [
                "name" => "Ibis Budget",
                "latitude"  => "52.32484",
                "longitude" => "4.79135" 
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.324913",
                "longitude" => "4.793572"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "52.337463",
                "longitude" => "4.81632"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "52.33388",
                "longitude" =>  "4.888951"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "52.335167",
                "longitude" => "4.913658"
            ],
            [
                "name" => "Ibis Styles",
                "latitude"  => "52.35809",
                "longitude" => "4.90061" 
            ],
            [
                "name" => "Mercure",
                "latitude"  => "52.361076",
                "longitude" => "4.893577"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.37002",
                "longitude" => "4.907611"
            ],
            [
                "name" => "Sofitel",
                "latitude"  => "52.37112",
                "longitude" => "4.89542"
            ],
            [
                "name" => "Sofitel",
                "latitude"  => "52.371006",
                "longitude" => "4.89567"
            ],
            [
                "name" => "INK",
                "latitude"  => "52.3755",
                "longitude" => "4.893"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.379665",
                "longitude" => "4.897333" 
            ],
            [
                "name" => "Ibis Styles",
                "latitude"  => "52.378166",
                "longitude" => "4.896667"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.392284",
                "longitude" => "4.850198"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "52.38734",
                "longitude" => "4.83533"
            ],
            [
                "name" => "Ibis Budget",
                "latitude"  => "52.42866",
                "longitude" => "4.84004"
            ],
            [
                "name" => "Ibis",
                "latitude"  => "52.08196",
                "longitude" => "5.08795"
            ],
            [
                "name" => "Mercure",
                "latitude"  => "52.15856",
                "longitude" => "5.379163"  
            ],
            [
                "name" => "Mercure",
                "latitude"  => "53.20164",
                "longitude" =>  "6.55457"
            ],
            [
                "name" => "Novotel",
                "latitude"  => "50.84494",
                "longitude" => "5.71831" 
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotels');
    }
}
