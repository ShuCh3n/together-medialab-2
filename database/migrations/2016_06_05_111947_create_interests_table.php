<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image');
            $table->timestamps();
        });

        DB::table('interests')->insert([
            [
                "name"  => "Beach",
                "image" => "beach.jpg"
            ],
            [
                "name" => "Sightseeing",
                "image" => "sightseeing.jpg"
            ],
            [
                "name" => "Nature",
                "image" => "nature.jpg"
            ],
            [
                "name" => "City",
                "image" => "city.jpg"
            ],
            [
                "name" => "Party",
                "image" => "party.jpg"
            ],
            [
                "name" => "Culture",
                "image" => "culture.jpg"
            ],
        ]);

        Schema::create('interest_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interest_id')->unsigned();
            $table->string('tag');

            $table->foreign('interest_id')
                ->references('id')
                ->on('interests')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('interest_tags');
        Schema::drop('interests');
    }
}
